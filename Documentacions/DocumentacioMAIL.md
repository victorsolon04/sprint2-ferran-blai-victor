# Documentació Mail

  -Primerament ens descarreguem el postfix

   ![](SPRINT2/foto1.png)

##### -Seguidament configurarem el postfix

  -Ecullirem quin tipus de configuario de mail voldrem

   ![](SPRINT2/foto2.png)

  -Ara escollim quin domini li posarem (en el nostre cas seria Kingdom.esp)

   ![](SPRINT2/foto3.png)

  -Continuem amb la configuarió

   ![](SPRINT2/foto4.png)

   ![](SPRINT2/foto5.png)

   ![](SPRINT2/foto6.png)

   ![](SPRINT2/foto7.png)

##### - instal·larem el dovecot

   ![](SPRINT2/foto8.png)

##### - Configuració

  -Amb la comanda "sudo nano /etc/profile"  modificarem l'arxiu al final i posarem " MAIL=/home/$USER/Maildir/"

   ![](SPRINT2/foto9.png)

  -Editarem l'arxiu "sudo nano /etc/dovecot/conf.d/10-mail.conf" on descomentarem la linea "mail_location = maildir:~/Maildir/" i posarem la barra que hi ha al final

  ![](SPRINT2/foto10.png)

  -reiniciem el dovecot

  ![](SPRINT2/foto11.png)

##### -Instal·lació i configuarió spamassasin

  ![](SPRINT2/foto12.png)

  -Configurarem l'arxiu "/etc/postfix/master.cf" on posarem les següents lineas que estan en verd

  ![](SPRINT2/foto21.png)

  ![](SPRINT2/foto22.png)

  -A l'arxiu "/etc/spamassasin/local.cf" fem l'idem d'abans

  ![](SPRINT2/foto23.png)


##### -Comprovació spamassasin

  ![](SPRINT2/foto24.png)
