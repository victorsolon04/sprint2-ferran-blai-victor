# DOCUMENTACIÓ DHCP
### Victor, Blai i Ferran

### Pas 1
## Instal·lació de paquets:
![](imatgesdns/dns1.png)

### Pas 2
## Obrir LOGS
![](imatgesdns/dns2.png)

### Pas 3
## Editem Directori named.conf.default-zones
Entrarem al fitxer amb nano:
![](imatgesdns/dns3.png)
i editarem el fitxer de la seguent forma:
![](imatgesdns/dns4.png)

### Pas 4
## farem copia d'arxiu db.local
![](imatgesdns/dns5.png)

### Pas 5
## Entrem a editar amb nano l'arxiu:
![](imatgesdns/dns5.png)
![](imatgesdns/dns6.png)
Substituirem les paraules <<localhost>> per kingdom.esp

### Pas 6
## Verificacións:
![](imatgesdns/dns7.png)

![](imatgesdns/dns8.png)

Tenim la zona activa
![](imatgesdns/dns.png)

![](imatgesdns/checkdns.png)
