# DOCUMENTACIÓ DHCP
### Victor, Blai i Ferran

### ANTIVIRS CLAMTK: (maquina client)
Al tenir una maquina client amb interfície podem fer l'instal·lació sense commandas, directament amb interficie gràfica.

![](imatgesAntivirus/1.1.png)

![](imatgesAntivirus/2.png)

### ANTIVIRS CLAMAV: (maquina servidor)

Al ser interfície que no és grafica, instal·larem l'antivirus per comandes i també l'executarem per comandes:

### Instal·lació:

![](imatgesAntivirus/clamservidor.png)

per poder executar l'antivirus hem d'executar:

![](imatgesAntivirus/clamfinal.png)

per detectar virus utilitzarem la commandas

#### clamsubmit -V
