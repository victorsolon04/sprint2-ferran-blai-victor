# DOCUMENTACIÓ FTP i SSL
### Victor, Blai i Ferran

# FTP
### Pas 1
## Instal·lació de paquets
![](imatgesftp/ftp1.png)

### Pas 2
## Configuracio vsftpd:
Entrarem al fitxer amb l'editor nano per fer la configuració:
![](imatgesftp/ftp2.png)

Descomentarém els seguents punts:
![](imatgesftp/ftp3.png)

![](imatgesftp/ftp4.png)

![](imatgesftp/ftp5.png)

### Pas 3
## Engaviem users:

Hem d'entrar al fitxer per editar amb nano:

![](imatgesftp/ftpgavies1.png)

Engaviem els usuaris de la nostra xarxa (blai, ferran, victor)

![](imatgesftp/ftpgavies2.png)

### Pas 4
## FileZilla

Al FileZilla afegirem un lloc:
![](imatgesftp/ftp6.png)

### Pas 5
## Crear directori "Documents"

Ens conectarem per ssh al nostre usuari
###### blai@192.168.1.3

i farem les seguents pases:
![](imatgesftp/ftp8.png)

dins crearem fitxers "prova1.txt" i "prova2.txt" per fer les comprovacions per veure si fem correctament les comunicacions de baixar i pujar arxius al servidor.
![](imatgesftp/ftpfinal.png)

# SSL


### Pas 1
## configuració /etc/vsfpd.conf

Entrarem amb l'editor nano per editar l'arxiu i posarem les seguents lineas al final del doc.
![](imatgesftp/ssl1.png)
##### Un cop configurat i guardat fem la seguent comanda:
sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout /etc/ssl/private/vsftpdserverkey.pem -out /etc/ssl/certs/vsftpdcertificate.pem -days 365
![](imatgesftp/comandassl.png)

### Pas 2
## Reiniciar serveis:
![](imatgesftp/checkokssl.png)

### Pas 3
## Ultimes comprovacións:
![](imatgesftp/instalssl.png)

![](imatgesftp/totokssl.png)
