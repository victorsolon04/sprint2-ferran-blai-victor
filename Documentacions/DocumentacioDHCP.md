# DOCUMENTACIÓ DHCP
### Victor, Blai i Ferran

### Pas 1
## Instal·lació de paquets.
Obrim un terminal on farem la seguent comanda per fer l'instal·lació de paquets:
![](imatgesdhcp/dhcp1.png)
### Pas 2
## Configuració fitxer netplan
Per entrar a configurar el fitxer utilitzarem la seguent comanda:
![](imatgesdhcp/netplan.png)
Configurarem el fitxer netplan amb nano i afegirem les seguents lineas de la forma que exposem al
exemple:

eth0: (o la xarxa que hagis d'afegir en el teu cas)

addresses:

dhcp4:

getway4:

nameservers:

search:

addresses:
![](imatgesdhcp/dhcp2.png)
### En aquest fitxer no podeu fer tabulacions.

### Pas 3
## LOGS:
Obrirem a un NOU TERMINAL els logs per anar veien el que fa la nostre configuració DHCP i podrem veure si tenim cap errada.
![](imatgesdhcp/logs.png)

### Pas 4
## APPLY:
Donarem la IP d'alta que hem asignat anteriorment.
![](imatgesdhcp/dhcpapply.png)

### Pas 5
## Configurar interfície:
Em d'afegir la interfície que hem configurat, en el nostre cas em configurat la eth0
![](imatgesdhcp/dhcp4.png)

### Pas 6
## configuració d'arxiu dhcpd.
Establir els paràmetres de la xarxa interna i comentar els paràmetres innecesaris.
![](imatgesdhcp/dhcp5.png)

### Pas 7
## Reiniciar serveis:
Reiniciarem les configuracions i les comprovarem per veure que s'han aplicat els canvis:

sudo service isc-dhcp-server restart (per fer el reset)

sudo rervice isc-dhcp-server status (per veeure la configuració actual)

![](imatgesdhcp/dhcp6.png)
