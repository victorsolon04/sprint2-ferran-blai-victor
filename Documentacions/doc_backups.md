# BACKUPS:

### Per a fer els backups del servidor hem triat aquest programa:

##### Hem fet una màquina nova on guardarem els backups de la nostre xarxa.
Entrem per ssh i creem el següent script:

![](imatgesbu/3.png)

Despres editem l'arxiu crontab -e:

![](imatgesbu/6.png)

I ja podem fer backups:

![](imatgesbu/4.png)

Quan es vulgui restaurar s'ha de fer el següent:

![](imatgesbu/5.png)

Desprès anem a configurar el client on guardarem els backups:

#### Primer de tot instal·lem ssh:

 -Sudo apt-get install ssh

#### Despres fem el manual de DHCP

 -I un cop tinguem connexió copiem els backups a la màquina que en aquest cas té la 192.168.1.7

 ![](imatgesbu/8.png)

-Per acabar programem els backups per que s'executin cada dia a les 5 del mati.

![](imatgesbu/11.png)

Ara programem script.sh


![](imatgesbu/12.png)
